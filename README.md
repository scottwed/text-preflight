# Text Preflight

This project was created as a tutorial on how to check text input files for non-UTF8 characters and implement basic multi-processing to expedite the analysis. 

## Day 1
* Run breakout_text.py to see how a string is composed of bytes
* Run corrupt_text_file.py _path_to_text_file_ to create a corrupted copy. 
* Run basic_scan.py _corrupted_filepath_ to report on non-utf8 characters.



### Credits:
Project icon based upon https://icons8.com
Dummy data based upon: https://www.generatedata.com


### License:
Copyright (C) 2020  Scott Wedekind

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see https://www.gnu.org/licenses/gpl-3.0.html
