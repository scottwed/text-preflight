# -*- encoding: utf-8 -*-
from pathlib import Path
from sys import argv


def check_file_for_non_utf8(filepath):
    """
    Checks the target file for non-UTF-8 characters.  Logs an error message for each occurrence, including
     the filename, line number, and column position of the character. Will also complain if the file
     can't be opened.
    :param filepath: pathlib.Path: The file to be checked.
    :return: int: A count of detected errors.
    """
    assert isinstance(filepath, Path)
    print("Checking", filepath.name, "for utf-8 compliance.")
    errors = []
    try:
        with filepath.open('r', errors='replace', encoding='utf-8') as f:
            for line_num, line in enumerate(f):
                for idx, c in enumerate(line):
                    print(c, end='')
                    if c == r'�':
                        error_msg = \
                            f"[{filepath.name} - line:{line_num+1} col:{idx}] Non UTF-8 character detected in text: "\
                            f"{line[idx-5:idx+5]}"
                        errors.append(error_msg)
    except IOError as e:
        error_msg = "Failed to open file: {} with error: {}".format(filepath, e)
        errors.append(error_msg)
    print("\nCompleted", filepath.name, "with", len(errors), "errors.")
    return tuple(errors)


if __name__ == '__main__':
    if len(argv) == 2:
        result = check_file_for_non_utf8(Path(argv[1]))
        print("Results: ")
        [print(r) for r in result]
    else:
        print("Please provide an input file to scan as the first parameter")
