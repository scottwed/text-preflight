# -*- encoding: utf-8 -*-
from pathlib import Path
from sys import argv
import random


def make_corrupt_copy(filepath, num_invalids=5):
    """
    Reads in a file, and injects a few
    :param filepath: pathlib.Path: The file to be cloned + corrupted.
    :param num_invalids:int: The number of invalid characters to inject
    :return: int: A count of detected errors.
    """
    assert isinstance(filepath, Path)
    assert isinstance(num_invalids, int)
    out_filepath = Path().joinpath(filepath.parent, "bad_" + filepath.name)
    print(F"Corrupting {filepath.name} as {out_filepath.absolute()}")

    """ Good code points - https://en.wikipedia.org/wiki/UTF-8
    Number Bits     First       Last        
    of     for      code        code 
    bytes  code     point       point
           point                            Byte 1 	    Byte 2 	    Byte 3 	    Byte 4 
    1 	    7 	    U+0000 	    U+007F 	    0xxxxxxx 			
    2 	   11 	    U+0080 	    U+07FF 	    110xxxxx 	10xxxxxx 		
    3 	   16 	    U+0800 	    U+FFFF 	    1110xxxx 	10xxxxxx 	10xxxxxx 	
    4 	   21 	    U+10000 	U+10FFFF 	11110xxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
    """

    # TODO: Enhance this to support corruption including multi-byte codepoints

    bad_bytes = []
    bad_bytes.extend(list(range(int(0x80), 257)))  # Byte max is int 256
    # print(bad_bytes)

    try:
        with filepath.open('r', encoding='utf-8') as f:
            line_count = len(f.readlines())
            print(f"Line count of input file: {line_count}")
            f.seek(0)
            random.seed()
            target_lines = sorted([random.randrange(0, line_count-1, 1) for i in range(num_invalids)])

            # 25% chance to place a bad byte at end of file (indicates interrupted write operation)
            corrupt_end_of_file = bool(random.randint(0, 3) == 3)
            if corrupt_end_of_file:
                target_lines = target_lines[:-1]
            print(target_lines, corrupt_end_of_file)

            with out_filepath.open('wb') as out_file:
                out_text = bytearray()
                for line_num, line in enumerate(f):
                    bad_idx = -1
                    if line_num in target_lines:
                        char_count = len(line)
                        bad_idx = random.randrange(0, char_count+1, 1)
                    for idx, c in enumerate(line):
                        if idx == bad_idx:
                            bad_byte = bad_bytes[random.randrange(0, len(bad_bytes)-1, 1)]
                            out_text.append(bad_byte)
                        else:
                            out_text.append(bytes(c, encoding='utf-8')[0])
                if corrupt_end_of_file:
                    bad_byte = bad_bytes[random.randrange(0, len(bad_bytes) - 1, 1)]
                    out_text = out_text[:-random.randrange(1, 10)]
                    out_text.append(bad_byte)
                out_file.write(out_text)

    except IOError as e:
        print(f"Failed to open file {filepath} with error: {e}")


if __name__ == '__main__':
    if len(argv) == 2:
        make_corrupt_copy(Path(argv[1]), 5)
    else:
        print("Required parameter is an existing text file to clone and corrupt.")