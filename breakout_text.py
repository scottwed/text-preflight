# -*- encoding: utf-8 -*-
my_text = '16310901-4203,�Olympia M. Hull�,True,Iceland'

# Converted to bytes -> string this is
# b'16310901-4203,\xef\xbf\xbdOlympia M. Hull\xef\xbf\xbd,True,Iceland'

# 'ï' = 239 = 11101111 = 0xef
# '#' = 191 = 10111111 = 0xbf
# '#' = 189 = 10111101 = 0xbd

print(bytes(my_text, 'utf-8'))

for b in bytes(my_text, encoding='utf-8'):
    # New characters start with 0### #### or 11## ####
    # This is crude sequence detection logic, based upon a UTF-8 requirement.
    if b & 128 == 0 or b & 192 == 192:
        new_char_seq = ''
    else:
        new_char_seq = 'CONTINUED FROM PRIOR BYTE'
    print(f"'{chr(b)}' = {b} = {b:08b} = {hex(b)} - {new_char_seq}")
